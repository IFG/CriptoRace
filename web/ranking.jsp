<%@page import="java.util.ArrayList"%>
<%@page import="br.dawii.model.Ranking"%>
<%@page import="java.util.List"%>
<%
    HttpSession sessao = request.getSession();
    //sessao.setAttribute(string, o);
    //sessao.getAttribute(string);
%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="formulario" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="scripts/jquery.validate.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="style/style.css">
        <title>CriptoRACE - V 2.0 - 2k17 - Ranking</title>
    </head>
    <body>
        <!-- início do corpo do site -->
        <div class="container">
            <div class="header clearfix">
                <h3 class="text-muted title"><a href="/criptoRace">CriptoRACE v 2.0 - 2k17</a></h3>  
            </div>
            <br />
            <div class="row">

                <div class="col-md-12 texto-preto">                     
                    <a href="submitAnswer.jsp" class="btn btn-primary btn-lg btn-block">Enviar Resposta</a>
                    <br />
                    <h3>Ranking Positivo</h3>
                    <table class="table">  
                        <%
                            int contador = 1;
                        %>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nick</th>
                                <th>Pontuação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="lista" items="${requestScope.rankingList}">
                                <tr>
                                    <th scope="row"><%= contador++%> º</th>
                                    <td>${lista.candidato.nick}</td>                     
                                    <td>${lista.pontuacao}</td>
                                </tr>                    
                            </c:forEach>
                        </tbody>
                        <h3>Ranking Negativo</h3>
                    </table> 
                    <table class="table">  
                        <%
                            int contador1 = 1;
                        %>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nick</th>
                                <th>Pontuação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="lista" items="${requestScope.rankingListNegativo}">
                                <tr>
                                    <th scope="row"><%= contador1++%> º</th>
                                    <td>${lista.candidato.nick}</td>                     
                                    <td>${lista.pontuacao}</td>
                                </tr>                    
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <br />
            <br />
            <footer class="footer">
                <p>&copy; 2017 pVH();</p>
            </footer>
        </div><!-- /container -->
    </body>
</html>
