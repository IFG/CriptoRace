<!--JSP java server page, é a pagina que mostra todo o html, para nao ficar misturado com o java no servlet-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2-Cadastro Candidatos</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="scripts/jquery.validate.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="style/style.css">

    </head>
    <body>
        <div class="container-fluid">
            <img class="img-responsive" src="img/logoG2.png" alt="CriptoRace">
            <form class="form-horizontal" action="" id="formCadastro" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nomeCompleto">Nome Completo:</label>
                    <div class="col-sm-10">
                        <input type="nomeCompleto" class="form-control" id="nomeCompleto" placeholder="Digite seu Nome Completo" name="nomeCompleto" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nick">Nick:</label>
                    <div class="col-sm-10">          
                        <input type="nick" class="form-control" id="nick" placeholder="Digite seu nick" name="nick">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="matricula">Matricula:</label>
                    <div class="col-sm-10">          
                        <input type="matricula" class="form-control" id="matricula" placeholder="Digite sua matricula" name="matricula">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="cpf">Cpf:</label>
                    <div class="col-sm-10">          
                        <input type="cpf" class="form-control" id="cpf" placeholder="Digite seu Cpf" name="cpf">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="user">User:</label>
                    <div class="col-sm-10">          
                        <input type="user" class="form-control" id="user" placeholder="Digite seu nome de Usuário" name="user">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Password:</label>
                    <div class="col-sm-10">          
                        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
                    </div>
                </div>
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Cadastrar</button>
                    </div>
                </div>
            </form>
        </div> <!-- // container -->
        <script>
            //submetendo um formulario com jquery!!
            $.validator.setDefaults({
                submitHandler: function () {
                    $("#formCadastro").attr('action', './cadastro');
                    $("#formCadastro").submit();
                }
            });
            // validate the comment form when it is submitted
            $("#formCadastro").validate({
                rules: {
                    nomeCompleto: {
                        required: true,
                        minlength: 3
                    },
                    nick: {
                        required: true
                    },
                    matricula: {
                        required: true
                    },
                    cpf: {
                        required: true
                    },
                    user: {
                        required: true
                    },
                    pwd: {
                        required: true
                    }
                },
                messages: {
                    nomeCompleto: {
                        required: "Por favor, informe seu nome",
                        minlength: "O nome deve ter pelo menos 3 caracteres"
                    },
                    nick: {
                        required: "É necessário informar um email"
                    },
                    matricula: {
                        required: "A mensagem não pode ficar em branco"
                    },
                    cpf: {
                        required: "A mensagem não pode ficar em branco"
                    },
                    user: {
                        required: "A mensagem não pode ficar em branco"
                    },
                    pwd: {
                        required: "A mensagem não pode ficar em branco"
                    }
                }
            });
        </script>
    </body>
</html>
