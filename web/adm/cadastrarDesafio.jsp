<!--JSP java server page, é a pagina que mostra todo o html, para nao ficar misturado com o java no servlet-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../scripts/jquery.validate.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../style/style.css">
    </head>
    <body>
        <div class="container-fluid">
            <img class="img-responsive" src="../img/logoG2.png" alt="CriptoRace">
            <form class="form-horizontal" action="" id="formCadastro" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2">Descriçao do Desafio</label>
                    <textarea class="col-sm-10" id="descricaoDesafio" name="descricaoDesafio" rows="3" ></textarea>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="pontuacao">Pontuação:</label>
                    <div class="col-sm-10">          
                        <input type="pontuacao" class="col-sm-2" id="pontuacao" placeholder="Digite a pontuação :" name="pontuacao" >
                    </div>
                </div>
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Cadastrar</button>
                    </div>
                </div>
            </form>
        </div> <!-- // container -->
        <script>
            //submetendo um formulario com jquery!!
            $.validator.setDefaults({
                submitHandler: function () {
                    $("#formCadastro").attr('action', '../cadastroDesafio');
                    $("#formCadastro").submit();
                }
            });
            // validate the comment form when it is submitted
            $("#formCadastro").validate({

                rules: {
                    descricaoDesafio: {
                        required: true,
                        minlength: 3
                    },
                    pontuacao: {
                        required: true
                    }
                },
                messages: {
                    descricaoDesafio: {
                        required: "Por favor, escreva o desafio",
                        minlength: "O nome deve ter pelo menos 3 caracteres"
                    },
                    pontuacao: {
                        required: "A Pontuação não pode ficar em branco"
                    }
                }
            });
        </script>
    </body>
</html>
