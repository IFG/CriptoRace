<%@page import="java.util.ArrayList"%>
<%@page import="br.dawii.model.Ranking"%>
<%@page import="java.util.List"%>
<%
    HttpSession sessao = request.getSession();
    //sessao.setAttribute(string, o);
    //sessao.getAttribute(string);
%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="formulario" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="<%=request.getContextPath()%>/scripts/jquery.validate.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <link href="<%=request.getContextPath()%>/style/style.css" type="text/css" rel="stylesheet" media="all">
    </head>
    <body>
        <div class="container-fluid">
            <img class="img-responsive" src="<%=request.getContextPath()%>/img/logoG2.png" alt="CriptoRace">
            <form class="form-horizontal" action="" id="formCadastro" method="post">
                <div class="h3">
                    <p><h3>Gestão de Usuários</h3></p> 
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nomeCandidato">Nome do Candidato:</label>
                    <div class="col-sm-10">          
                        <input type="nomeCandidato" class="col-sm-10" id="nomeCandidato" placeholder="Digite o Nome do Candidato :" name="nomeCandidato" >
                    </div>
                </div> 
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default" id="BuscaNome">Busca</button>
                    </div>
                </div>

                <div class="col-md-12 texto-preto">                     

                    <br />
                    <h3>Candidatos</h3>
                    <table class="table">  
                        <%
                            int contador = 1;
                        %>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nick</th>
                                <th>Nome</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="lista" items="${requestScope.candidatosList}">
                                <tr>
                                    <th scope="row"><%= contador++%> º</th>
                                    <td>${lista.nick}</td>                   
                                    <td>${lista.nomeCompleto}</td>                     
                                </tr>                    
                            </c:forEach>
                        </tbody>
                    </table> 
                </div>

            </form>
        </div> <!-- // container -->
        <script>
            //submetendo um formulario com jquery!!
            $.validator.setDefaults({
                submitHandler: function () {

                    $("#formCadastro").attr('action', '../buscaNome');
                    $("#formCadastro").submit();

                }
            });
            // validate the comment form when it is submitted
            $("#formCadastro").validate({

                rules: {
                    descricaoDesafio: {
                        required: true,
                        minlength: 3
                    },
                    pontuacao: {
                        required: true
                    }
                },
                messages: {
                    descricaoDesafio: {
                        required: "Por favor, escreva o desafio",
                        minlength: "O nome deve ter pelo menos 3 caracteres"
                    },
                    pontuacao: {
                        required: "A Pontuação não pode ficar em branco"
                    }
                }
            });
        </script>
    </body>
</html>
