<!--JSP java server page, é a pagina que mostra todo o html, para nao ficar misturado com o java no servlet-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2-Cadastro Candidatos</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="<%=request.getContextPath()%>scripts/jquery.validate.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/style/style.css">

    </head>
    <body>
        <div class="container-fluid">
            <img class="img-responsive" src="<%=request.getContextPath()%>/img/logoG2.png" alt="CriptoRace">
            <form class="form-horizontal" action="./cadastrarEquipe" method="POST">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nomeEquipe">Nome da Equipe:</label>
                    <div class="col-sm-10">
                        <input type="nomeEquipe" class="form-control" id="nomeCompleto" placeholder="Digite o Nome da Equipe" name="nomeEquipe" >
                    </div>
                </div>
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Cadastrar</button>
                    </div>
                </div>
            </form>
        </div> <!-- // container -->
        <script>
            
            // validate the comment form when it is submitted
            $("#formCadastro").validate({
                rules: {
                    nomeEquipe: {
                        required: true,
                        minlength: 3
                    }
                },

                messages: {
                    nomeEquipe: {
                        required: "Por favor, informe seu Nome da Equipe",
                        minlength: "O nome deve ter pelo menos 3 caracteres"
                    }
                }
            });
        </script>
    </body>
</html>
