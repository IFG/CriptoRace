<%
    HttpSession sessao = request.getSession(true);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../style/style.css">
        <script>
            <%
                String resp = (String) request.getAttribute("LoginErrado");
                if (resp != null) {
                    if (resp.equalsIgnoreCase("Sim")) {
                        out.println("alert('Usuário ou Senha Inválido!');");
                    }
                }
            %>
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <img class="img-responsive" src="../img/logoG2.png" alt="CriptoRace">
            <form class="form-horizontal" action="./loginAdministradores" method="POST">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="usuario">Usuário do Administrador:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="usuario" placeholder="Nome de Usuário Administrador" name="usuario">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pwd">Senha do Administrador:</label>
                    <div class="col-sm-10">          
                        <input type="password" class="form-control" id="pwd" placeholder="Senha do Usuário Administrador" name="pwd">
                    </div>
                </div>
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-12">
                        <button type="submit" class="btn btn-default">Entrar</button>
                    </div>
                </div>
            </form>
        </div> <!-- // container -->
    </body>
</html>
