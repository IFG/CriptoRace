<%@page import="java.util.ArrayList"%>
<%@page import="br.dawii.model.Ranking"%>
<%@page import="java.util.List"%>
<%
    HttpSession sessao = request.getSession();
    //sessao.setAttribute(string, o);
    //sessao.getAttribute(string);
%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="formulario" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="<%=request.getContextPath()%>/scripts/jquery.validate.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <link href="<%=request.getContextPath()%>/style/style.css" type="text/css" rel="stylesheet" media="all">
    </head>
    <body>
        <div class="container-fluid">
            <img class="img-responsive" src="<%=request.getContextPath()%>/img/logoG2.png" alt="CriptoRace">
            <form class="form-horizontal" action="" id="formCadastro" method="post">
                <div class="col-md-12 texto-preto">
                    <div class="form-group">        
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default" id="listaTodos">Lista Todos</button>
                        </div>
                    </div>
                </div>
            </form>
            <br />
            <h3>Candidatos</h3>
            <form class="form-horizontal" action="" id="formBanir" method="post">
                <div class="col-md-12 texto-preto">
                    <table class="table">  
                        <%
                            int contador = 1;
                        %>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nick</th>
                                <th>Nome</th> 
                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach var="lista" items="${requestScope.candidatosList}">
                                <tr>
                                    <td>${lista.idCandidato}</td>

                            <input type="hidden" name="idCandidato" value="${lista.idCandidato}" id="idCandidato">
                            <td>${lista.nick}</td>                   
                            <td>${lista.nomeCompleto}</td>
                            <td><button type="submit" onclick="banirCandidato()" class="btn btn-default" id="banir">Banir</button></td>
                            </tr>                    
                        </c:forEach>
                        </tbody>
                    </table> 
                </div>
            </form>
        </div> <!-- // container -->
        <script>
            //submetendo um formulario com jquery!!
            $.validator.setDefaults({
                submitHandler: function () {
                    $("#formCadastro").attr('action', '../listaTodos');
                    $("#formCadastro").submit();
                }

            });

            function banirCandidato() {
                $("#formBanir").attr('action', './banirCandidato');
                $("#formBanir").submit();
            }

            // validate the comment form when it is submitted
            $("#formCadastro").validate({

                rules: {
                    descricaoDesafio: {
                        required: true,
                        minlength: 3
                    },
                    pontuacao: {
                        required: true
                    }
                },
                messages: {
                    descricaoDesafio: {
                        required: "Por favor, escreva o desafio",
                        minlength: "O nome deve ter pelo menos 3 caracteres"
                    },
                    pontuacao: {
                        required: "A Pontuação não pode ficar em branco"
                    }
                }
            });
        </script>
    </body>
</html>
