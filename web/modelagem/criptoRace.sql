-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 24, 2017 at 09:26 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `criptoRace`
--

-- --------------------------------------------------------

--
-- Table structure for table `acertoCandidato`
--

CREATE TABLE `acertoCandidato` (
  `idAcertos` int(11) NOT NULL,
  `idDesafio` int(11) DEFAULT NULL,
  `idRespostaSubmetida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acertoCandidato`
--

INSERT INTO `acertoCandidato` (`idAcertos`, `idDesafio`, `idRespostaSubmetida`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `candidato`
--

CREATE TABLE `candidato` (
  `idCandidato` int(11) NOT NULL,
  `nomeCompleto` varchar(100) NOT NULL,
  `nick` varchar(100) DEFAULT NULL,
  `matriculaIFG` varchar(30) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `user` varchar(45) NOT NULL,
  `passWd` varchar(200) NOT NULL,
  `idContest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidato`
--

INSERT INTO `candidato` (`idCandidato`, `nomeCompleto`, `nick`, `matriculaIFG`, `cpf`, `user`, `passWd`, `idContest`) VALUES
(1, 'usuário teste', 'tester', '99999999', NULL, 'tester', 'tester', 1),
(2, 'Usuário teste 2', 'tester2', '888', NULL, 'tester2', 'tester2', 1),
(3, 'Usuário teste 3', 'tester3', '4444', NULL, 'tester3', 'tester3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contest`
--

CREATE TABLE `contest` (
  `idContest` int(11) NOT NULL,
  `nomeContest` varchar(200) NOT NULL,
  `status` enum('Ativo','Inativo') DEFAULT NULL,
  `loc` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contest`
--

INSERT INTO `contest` (`idContest`, `nomeContest`, `status`, `loc`) VALUES
(1, 'teste', 'Ativo', 'ifg-inhumas');

-- --------------------------------------------------------

--
-- Table structure for table `desafios`
--

CREATE TABLE `desafios` (
  `idDesafio` int(11) NOT NULL,
  `descricaoDesafio` varchar(300) NOT NULL,
  `idContest` int(11) NOT NULL,
  `status` enum('Ativo','Inativo') NOT NULL,
  `pontuacao` int(1) NOT NULL,
  `tipoDesafio` enum('Individual','Equipe') DEFAULT NULL,
  `idEquipe` int(11) DEFAULT NULL COMMENT 'Id da equipe criadora, para o caso de desafios criados por equipes. Caso seja criado pelo adm, fica em branco.\n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desafios`
--

INSERT INTO `desafios` (`idDesafio`, `descricaoDesafio`, `idContest`, `status`, `pontuacao`, `tipoDesafio`, `idEquipe`) VALUES
(1, 'teste', 1, 'Ativo', 1, 'Individual', NULL),
(2, 'teste 2', 1, 'Ativo', 2, 'Individual', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE `equipe` (
  `idEquipe` int(11) NOT NULL,
  `nomeEquipe` varchar(45) NOT NULL,
  `idContest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`idEquipe`, `nomeEquipe`, `idContest`) VALUES
(1, 'Testadores', 1);

-- --------------------------------------------------------

--
-- Table structure for table `equipe_has_candidato`
--

CREATE TABLE `equipe_has_candidato` (
  `equipe_idEquipe` int(11) NOT NULL,
  `candidato_idCandidato` int(11) NOT NULL,
  `tipo` enum('lider','membro') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='representa  participação de um candidato em uma equipe, em um contest';

--
-- Dumping data for table `equipe_has_candidato`
--

INSERT INTO `equipe_has_candidato` (`equipe_idEquipe`, `candidato_idCandidato`, `tipo`) VALUES
(1, 1, 'lider'),
(1, 2, 'membro');

-- --------------------------------------------------------

--
-- Table structure for table `inativos`
--

CREATE TABLE `inativos` (
  `idInativos` int(11) NOT NULL,
  `idDesafio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `respDesafio`
--

CREATE TABLE `respDesafio` (
  `idResp` int(11) NOT NULL,
  `resposta` longtext NOT NULL,
  `idDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `respDesafio`
--

INSERT INTO `respDesafio` (`idResp`, `resposta`, `idDesafio`) VALUES
(1, 'teste', 1),
(2, 'teste2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `respostaSubmetida`
--

CREATE TABLE `respostaSubmetida` (
  `idRespSubmetida` int(11) NOT NULL,
  `respostaSubmetida` longtext,
  `idCandidato` int(11) DEFAULT NULL,
  `idDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `respostaSubmetida`
--

INSERT INTO `respostaSubmetida` (`idRespSubmetida`, `respostaSubmetida`, `idCandidato`, `idDesafio`) VALUES
(1, 'teste', 1, 1),
(2, 'teste2', 2, 1),
(3, 'teste2', 1, 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_candidato`
--
CREATE TABLE `view_candidato` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(45)
,`passWd` varchar(200)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ranking`
--
CREATE TABLE `view_ranking` (
`id` int(11)
,`nick` varchar(100)
,`nomeCompleto` varchar(100)
,`totalPontos` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Structure for view `view_candidato`
--
DROP TABLE IF EXISTS `view_candidato`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_candidato`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` ;

-- --------------------------------------------------------

--
-- Structure for view `view_ranking`
--
DROP TABLE IF EXISTS `view_ranking`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ranking`  AS  select `candidato`.`idCandidato` AS `id`,`candidato`.`nick` AS `nick`,`candidato`.`nomeCompleto` AS `nomeCompleto`,sum(`desafios`.`pontuacao`) AS `totalPontos` from (((`acertoCandidato` join `respostaSubmetida` on((`respostaSubmetida`.`idRespSubmetida` = `acertoCandidato`.`idRespostaSubmetida`))) join `desafios` on((`desafios`.`idDesafio` = `acertoCandidato`.`idDesafio`))) join `candidato` on((`candidato`.`idCandidato` = `respostaSubmetida`.`idCandidato`))) group by `candidato`.`idCandidato` order by `totalPontos` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  ADD PRIMARY KEY (`idAcertos`),
  ADD KEY `fk_acertoCandidato_desafios` (`idDesafio`),
  ADD KEY `fk_acertoCandidato_respostaSubmetida` (`idRespostaSubmetida`);

--
-- Indexes for table `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`idCandidato`),
  ADD UNIQUE KEY `matriculaIFG` (`matriculaIFG`),
  ADD UNIQUE KEY `matriculaIFG_2` (`matriculaIFG`),
  ADD KEY `cpf_2` (`cpf`),
  ADD KEY `fk_candidato_contest1_idx` (`idContest`);

--
-- Indexes for table `contest`
--
ALTER TABLE `contest`
  ADD PRIMARY KEY (`idContest`);

--
-- Indexes for table `desafios`
--
ALTER TABLE `desafios`
  ADD PRIMARY KEY (`idDesafio`),
  ADD KEY `fk_desafios_contest` (`idContest`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`idEquipe`),
  ADD KEY `fk_equipe_contest1_idx` (`idContest`);

--
-- Indexes for table `equipe_has_candidato`
--
ALTER TABLE `equipe_has_candidato`
  ADD PRIMARY KEY (`equipe_idEquipe`,`candidato_idCandidato`),
  ADD KEY `fk_equipe_has_candidato_candidato1_idx` (`candidato_idCandidato`),
  ADD KEY `fk_equipe_has_candidato_equipe1_idx` (`equipe_idEquipe`);

--
-- Indexes for table `inativos`
--
ALTER TABLE `inativos`
  ADD PRIMARY KEY (`idInativos`),
  ADD KEY `fk_inativos_desafios` (`idDesafio`);

--
-- Indexes for table `respDesafio`
--
ALTER TABLE `respDesafio`
  ADD PRIMARY KEY (`idResp`),
  ADD KEY `fk_respDesafio_desafios` (`idDesafio`);

--
-- Indexes for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  ADD PRIMARY KEY (`idRespSubmetida`),
  ADD KEY `fk_respostaSubmetida_candidato` (`idCandidato`),
  ADD KEY `fk_respostaSubmetida_desafios` (`idDesafio`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  MODIFY `idAcertos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `candidato`
--
ALTER TABLE `candidato`
  MODIFY `idCandidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contest`
--
ALTER TABLE `contest`
  MODIFY `idContest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `desafios`
--
ALTER TABLE `desafios`
  MODIFY `idDesafio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `idEquipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inativos`
--
ALTER TABLE `inativos`
  MODIFY `idInativos` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `respDesafio`
--
ALTER TABLE `respDesafio`
  MODIFY `idResp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  MODIFY `idRespSubmetida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  ADD CONSTRAINT `fk_acertoCandidato_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`),
  ADD CONSTRAINT `fk_acertoCandidato_respostaSubmetida` FOREIGN KEY (`idRespostaSubmetida`) REFERENCES `respostaSubmetida` (`idRespSubmetida`);

--
-- Constraints for table `candidato`
--
ALTER TABLE `candidato`
  ADD CONSTRAINT `fk_candidato_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `desafios`
--
ALTER TABLE `desafios`
  ADD CONSTRAINT `fk_desafios_contest` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `fk_equipe_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `equipe_has_candidato`
--
ALTER TABLE `equipe_has_candidato`
  ADD CONSTRAINT `fk_equipe_has_candidato_candidato1` FOREIGN KEY (`candidato_idCandidato`) REFERENCES `candidato` (`idCandidato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_equipe_has_candidato_equipe1` FOREIGN KEY (`equipe_idEquipe`) REFERENCES `equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inativos`
--
ALTER TABLE `inativos`
  ADD CONSTRAINT `fk_inativos_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `respDesafio`
--
ALTER TABLE `respDesafio`
  ADD CONSTRAINT `fk_respDesafio_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`);

--
-- Constraints for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  ADD CONSTRAINT `fk_respostaSubmetida_candidato` FOREIGN KEY (`idCandidato`) REFERENCES `candidato` (`idCandidato`),
  ADD CONSTRAINT `fk_respostaSubmetida_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
