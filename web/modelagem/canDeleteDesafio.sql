CREATE DEFINER=`root`@`localhost` FUNCTION `canDeleteDesafio`(idD int(11)) RETURNS tinyint(1)
BEGIN
	DECLARE c1 int(11);
    DECLARE c2 int(11);
    DECLARE c3 int(11);
    set c1 = (select count(*) from acertoCandidato where idDesafio = idD);
    set c2 = (select count(*) from respostaSubmetida where idDesafio = idD);
    set c3 = (select count(*) from respDesafio where idDesafio = idD);
	if ((c1>0)or (c2>0) or (c3>0))then
		return false;
	end if;
		RETURN true;
END