-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 06, 2017 at 10:42 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `criptoRace`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_atribui_pontuacao` (`idD` INT(11), `idRS` INT(11))  BEGIN
    insert into criptoRace.acertoCandidato values (null, idD, idRS);    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_cadastraCandidato` (IN `nC` VARCHAR(100), IN `nick` VARCHAR(100), IN `matricula` VARCHAR(30), IN `cpf` VARCHAR(15), IN `usuario` VARCHAR(45), IN `pass` VARCHAR(200), IN `idC` INT, OUT `inserido` INT)  BEGIN
	INSERT INTO `candidato`
		(nomeCompleto,nick,matriculaIFG,cpf,user,passWd,idContest)
        values
        (nC,nick,matricula,cpf,usuario,pass,idC);
        SET inserido = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_inativaDesafio` (`idD` INT(11))  BEGIN
	update desafios set status = "Inativo" where idDesafio = idD;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_procura_candidato` (`termoBusca` VARCHAR(400), `idC` INT(11))  BEGIN
	SELECT * FROM `candidato` WHERE ((nomeCompleto like CONCAT('%',termoBusca,'%')) or (nick like CONCAT('%',termoBusca,'%')) or (matriculaIFG like CONCAT('%',termoBusca,'%')) or (cpf like CONCAT('%',termoBusca,'%')) and (idContest=idC));
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_procura_candidatos_sem_equipe` (`idC` INT(11), `nome` VARCHAR(30))  BEGIN
	SELECT * FROM candidato where (
		idCandidato not in (select candidato_idCandidato from equipe_has_candidato)
        and
        idContest = idC
        and nick like concat('%',nome,'%')
        );
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_view_respostas_desafio` (`idD` INT(11))  BEGIN
	select * from respDesafio where idDesafio = idD;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `canDeleteDesafio` (`idD` INT(11)) RETURNS TINYINT(1) BEGIN
	DECLARE c1 int(11);
    DECLARE c2 int(11);
    DECLARE c3 int(11);
    set c1 = (select count(*) from acertoCandidato where idDesafio = idD);
    set c2 = (select count(*) from respostaSubmetida where idDesafio = idD);
    set c3 = (select count(*) from respDesafio where idDesafio = idD);
	if ((c1>0)or (c2>0) or (c3>0))then
		return false;
	end if;
		RETURN true;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_altera_descricao_desafio` (`novaDescricao` VARCHAR(300), `idD` INT(11)) RETURNS INT(11) BEGIN
	update desafios set descricaoDesafio = novaDescricao where idDesafio = idD;
RETURN ROW_COUNT();
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_altera_status_desafio` (`novoStatus` VARCHAR(20), `idD` INT(11)) RETURNS INT(11) BEGIN
	if(novoStatus = "Ativo" or novoStatus = "Inativo") then
		update desafios set status = novoStatus where idDesafio = idD;
        RETURN ROW_COUNT();
    else
		return 0;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_altera_tipo_desafio` (`novoTipo` VARCHAR(30), `idD` INT(11)) RETURNS INT(11) BEGIN
if(novoTipo = "Individual" or novoTipo = "Equipe") then
	update desafios set tipoDesafio = novoTipo where idDesafio = idD;
    return ROW_COUNT();
else
		return 0;
end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_associa_candidato_equipe` (`idEquipe` INT(11), `idCandidato` INT(11), `tipo` VARCHAR(30)) RETURNS INT(11) BEGIN
	/*vendo se o candidato informado já não é membro de alguma equipe retorno -1 indica que ele já participa*/
	if(SELECT count(*) FROM equipe_has_candidato where candidato_idCandidato = idCandidato)>0 then
		return -1;
    else
		/*vendo se não está tentando inserir um líder que já é líder em outro grupo*/
		if(SELECT count(*) FROM equipe_has_candidato where (equipe_has_candidato.candidato_idCandidato = idCandidato and equipe_has_candidato.tipo='lider')>0 and tipo='lider')then
			return -2;           
        else 
			/*garantindo que o primeiro membro da equipe seja o líder*/ 
			if((SELECT count(*) FROM equipe_has_candidato where (equipe_idEquipe = idEquipe))=0 and (tipo<>'lider')) then
				return -3;
            elseif((SELECT count(*) FROM `equipe_has_candidato` where equipe_idEquipe=idEquipe and tipo='lider')>0)then
				/*checando se o grupo já não tem líder*/
                return -4;
            elseif((SELECT count(*) FROM `equipe_has_candidato` where equipe_idEquipe = idEquipe)>=3)then
				return -5; /*limite de 3 membros já inseridos*/
            else
				/*se chegou aqui é pq pode cadastrar*/
				insert into equipe_has_candidato values (idEquipe,idCandidato,tipo);
                return ROW_COUNT();
            end if;  			
        end if;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastraAdm` (`nC` VARCHAR(100), `nick` VARCHAR(100), `matricula` VARCHAR(30), `cpf` VARCHAR(15), `usuario` VARCHAR(400), `pass` VARCHAR(400), `uL` VARCHAR(400), `pL` VARCHAR(400)) RETURNS INT(11) BEGIN
	if(hasAdm(uL,pL)>0)then
        if((SELECT count(*) FROM administrador WHERE (user = usuario))=0)then
			INSERT INTO `administrador`
				(nomeCompleto,nick,matriculaIFG,cpf,user,passWd)
				values
				(nC,nick,matricula,cpf,usuario,pass);
				return LAST_INSERT_ID();
        else
			return -1;
        end if;    
     else
			return 0;
     end if;   
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastraCandidato` (`nC` VARCHAR(100), `nick` VARCHAR(100), `matricula` VARCHAR(30), `cpf` VARCHAR(15), `usuario` VARCHAR(45), `pass` VARCHAR(200), `idC` INT) RETURNS INT(11) BEGIN
if((select count(*) from candidato where (matriculaIFG=matricula))=0)then
	if((select count(*) from candidato where (user=usuario))=0)then
		INSERT INTO `candidato`
		(nomeCompleto,nick,matriculaIFG,cpf,user,passWd,idContest)
		values
		(nC,nick,matricula,cpf,usuario,pass,idC);
		return LAST_INSERT_ID();
	else
		return 0;
	end if;
else
	return 0;
end if; 
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastra_desafio` (`descD` VARCHAR(300), `idC` INT(11), `pontuacao` INT(10), `tpDesafio` VARCHAR(20), `idEquipe` INT(11)) RETURNS INT(11) BEGIN
	insert into desafios values
    (null,
	descD,
	idC,
    "Inativo",
	pontuacao,
	tpDesafio,
	idEquipe);
RETURN LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastra_desafio_equipe` (`descD` VARCHAR(300), `idC` INT(11), `pontuacao` INT(10), `idEquipe` INT(11)) RETURNS INT(11) BEGIN
	if(pontuacao not in (1,2,3))then
		return -1;
    elseif(function_valida_lider(idC)=0)then
		return -2;
    else   
        return function_cadastra_desafio(descD,idC,pontuacao,'Equipe',idEquipe);
    end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastra_equipe` (`nmEquipe` VARCHAR(100), `idC` INT(11)) RETURNS INT(11) BEGIN
	if((select count(*) from equipe where nomeEquipe = nmEquipe and idContest=idC)=0)then
		insert into equipe values (null, nmEquipe, idC);
        return LAST_INSERT_ID();
    else
		return -1;    
    end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastra_respDesafio` (`nResp` LONGTEXT, `idD` INT(11)) RETURNS INT(11) BEGIN
	insert into respDesafio values (null, nResp, idD);
RETURN LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_checa_respostas_candidato` (`idC` INT(11), `resp` LONGTEXT) RETURNS INT(11) BEGIN
DECLARE cont int(11);
	set cont = (SELECT count(*) as 'total' FROM `respostaSubmetida` inner join 
		acertoCandidato on acertoCandidato.idRespostaSubmetida = 
		respostaSubmetida.idRespSubmetida 
		WHERE (respostaSubmetida.respostaSubmetida= resp and 
		respostaSubmetida.idCandidato=idC));
	return cont;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_get_equipe_do_candidato` (`idCandidato` INT(11)) RETURNS VARCHAR(100) CHARSET latin1 BEGIN
	DECLARE nomeEquipe varchar(100);
    set nomeEquipe = (select concat(equipe.nomeEquipe, '-',equipe_has_candidato.tipo) as 'equipe' from equipe_has_candidato 
			inner join candidato on candidato.idCandidato = 
            equipe_has_candidato.candidato_idCandidato INNER join equipe on 
            equipe.idEquipe = equipe_has_candidato.equipe_idEquipe where 
            equipe_has_candidato.candidato_idCandidato=idCandidato);
RETURN nomeEquipe;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_get_pontuacao` (`idCandidato` INT(11)) RETURNS INT(11) BEGIN
	return (SELECT 
        SUM(`criptoRace`.`desafios`.`pontuacao`) AS `Pontuação`
    FROM
        (((`criptoRace`.`acertoCandidato`
        JOIN `criptoRace`.`respostaSubmetida` ON ((`criptoRace`.`respostaSubmetida`.`idRespSubmetida` = `criptoRace`.`acertoCandidato`.`idRespostaSubmetida`)))
        JOIN `criptoRace`.`desafios` ON ((`criptoRace`.`desafios`.`idDesafio` = `criptoRace`.`acertoCandidato`.`idDesafio`)))
        JOIN `criptoRace`.`candidato` ON ((`criptoRace`.`candidato`.`idCandidato` = `criptoRace`.`respostaSubmetida`.`idCandidato`)))
    where candidato.idCandidato=idCandidato
    GROUP BY `criptoRace`.`candidato`.`idCandidato`);
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_remove_desafio` (`idD` INT(11)) RETURNS INT(11) BEGIN
	if (canDeleteDesafio(idD))then
		delete from desafios where idDesafio = idD;
        RETURN ROW_COUNT();
    else    
		RETURN -1;
    end if;    
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_salva_msg` (`fullstr` VARCHAR(140), `idC` INT(11)) RETURNS INT(11) BEGIN
DECLARE frase varchar(400) default '';
set frase = function_valida_msg(fullstr);
	if (frase= 'frase negada por palavras ofensivas!')then
		return -1;
    else
		insert into mensagens values (null, frase, idC);
        return LAST_INSERT_ID();
    end if;    
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_salva_resposta_submetida` (`resp` LONGTEXT, `idC` INT(11)) RETURNS INT(11) BEGIN
	insert into respostaSubmetida values (null, resp, idC, null);
RETURN (SELECT LAST_INSERT_ID());
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_valida_lider` (`idC` INT(11)) RETURNS INT(11) BEGIN
declare idE int(11) default 0;
	set idE= (SELECT equipe_idEquipe as 'idEquipe' FROM `equipe_has_candidato` inner join candidato on candidato.idCandidato = equipe_has_candidato.candidato_idCandidato
			where idCandidato = idC and equipe_has_candidato.tipo = 'lider');
	if(idE IS null)then
		return 0;
    else    
		return idE;
    end if;    
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_valida_msg` (`fullstr` VARCHAR(140)) RETURNS VARCHAR(200) CHARSET latin1 BEGIN
	DECLARE inipos INTEGER;
    DECLARE endpos INTEGER;
    DECLARE maxlen INTEGER;
    DECLARE item VARCHAR(100);
    DECLARE delim VARCHAR(1);
    DECLARE saida VARCHAR(200) default '';
    DECLARE contador int(11) default 0;

    SET delim = ' ';
    SET inipos = 1;
    SET fullstr = CONCAT(fullstr, delim);
    SET maxlen = LENGTH(fullstr);

    REPEAT
        SET endpos = LOCATE(delim, fullstr, inipos);
        SET item =  SUBSTR(fullstr, inipos, endpos - inipos);

        IF (item <> '' AND item IS NOT NULL) THEN  
            if((select count(*) from censura where palavra like item)>0)then
				set item = '#$@#@$@#';
				set contador = contador+1;
            end if;
            set saida = concat(saida, ' ' ,item);
        END IF;
        SET inipos = endpos + 1;
    UNTIL inipos >= maxlen END REPEAT;
    if(contador<=3)then
		return saida;
    else
		return 'frase negada por palavras ofensivas!';
    end if;    
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_valida_resposta` (`resp` LONGTEXT, `idC` INT(11)) RETURNS INT(11) BEGIN	
 DECLARE pontos int(11);
 DECLARE idD int(11);
 DECLARE idRS int(11);
 DECLARE t varchar(100);
 DECLARE idContest int(11);
 
 set idContest = (SELECT contest.idContest FROM candidato inner join contest on contest.idContest =candidato.idContest where candidato.idCandidato = idC);
 set pontos = (pegaPontuacao(resp, idContest));
 set idRS = (function_salva_resposta_submetida(resp, idC)); 
  
 if(pontos <> null or pontos > 0)then
	set idD = (pegaIdDesafio(resp, idContest));
    /*return function_checa_respostas_candidato(idC,resp);*/
    if (function_checa_respostas_candidato(idC,resp)>0)then
        set pontos = -1;    
	else		
        call procedure_atribui_pontuacao(idD,idRS);  
	end if;
	if(isInactively(idD))then		
        call procedure_inativaDesafio(idD);        
    end if;
	RETURN pontos;
 else
	RETURN 0;
 end if;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `hasAdm` (`u` VARCHAR(400), `s` VARCHAR(400)) RETURNS INT(11) BEGIN
	return (select count(*) from administrador where (user=u and passWd=s));

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `inativaDesafio` (`idD` INT(11)) RETURNS INT(11) BEGIN	
	update desafios set status = "Inativo" where idDesafio = idD;
RETURN 1;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `isInactively` (`idD` INT(11)) RETURNS TINYINT(1) BEGIN
	if ((select count(*) from inativos where idDesafio = idD)>0)then
		return true;
    else
		return false;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `new_function` (`i` INT) RETURNS INT(11) BEGIN

RETURN i;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `pegaIdDesafio` (`resp` LONGTEXT, `idC` INT(11)) RETURNS INT(11) BEGIN
DECLARE idD int(11);
    set idD = (
			select desafios.idDesafio from respDesafio inner join desafios on desafios.idDesafio = respDesafio.idDesafio
			where (desafios.status= "Ativo" and respDesafio.resposta = resp and desafios.idContest = idC) Limit 1
            );
RETURN idD;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `pegaPontuacao` (`resp` LONGTEXT, `idC` INT(11)) RETURNS INT(11) BEGIN
	DECLARE pontuacao int(11);
    set pontuacao =(select desafios.pontuacao from respDesafio inner join desafios on desafios.idDesafio = respDesafio.idDesafio
		where (desafios.status= "Ativo" and respDesafio.resposta = resp and desafios.idContest = idC) Limit 1);
RETURN pontuacao;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `acertoCandidato`
--

CREATE TABLE `acertoCandidato` (
  `idAcertos` int(11) NOT NULL,
  `idDesafio` int(11) DEFAULT NULL,
  `idRespostaSubmetida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acertoCandidato`
--

INSERT INTO `acertoCandidato` (`idAcertos`, `idDesafio`, `idRespostaSubmetida`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3),
(4, 1, 23),
(7, 6, 71),
(8, 6, 77),
(9, 5, 80);

-- --------------------------------------------------------

--
-- Table structure for table `administrador`
--

CREATE TABLE `administrador` (
  `idCandidato` int(11) NOT NULL,
  `nomeCompleto` varchar(100) NOT NULL,
  `nick` varchar(100) DEFAULT NULL,
  `matriculaIFG` varchar(30) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `user` varchar(400) NOT NULL,
  `passWd` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrador`
--

INSERT INTO `administrador` (`idCandidato`, `nomeCompleto`, `nick`, `matriculaIFG`, `cpf`, `user`, `passWd`) VALUES
(1, 'Tester', 'testador', '000', '', 'admin', 'admin'),
(2, 'TESTE2', 'TESTE2', '0', '44', 'teste', 'teste'),
(5, 'TESTE2', 'TESTE2', '03', '44', 'testse', 'teste');

-- --------------------------------------------------------

--
-- Table structure for table `candidato`
--

CREATE TABLE `candidato` (
  `idCandidato` int(11) NOT NULL,
  `nomeCompleto` varchar(100) NOT NULL,
  `nick` varchar(100) DEFAULT NULL,
  `matriculaIFG` varchar(30) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `user` varchar(400) DEFAULT NULL,
  `passWd` varchar(400) DEFAULT NULL,
  `idContest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidato`
--

INSERT INTO `candidato` (`idCandidato`, `nomeCompleto`, `nick`, `matriculaIFG`, `cpf`, `user`, `passWd`, `idContest`) VALUES
(1, 'usuário teste', 'tester', '99999999', NULL, 'tester', 'tester', 1),
(2, 'Usuário teste 2', 'tester2', '888', NULL, 'tester2', 'tester2', 1),
(3, 'Usuário teste 3', 'tester3', '4444', NULL, 'tester3', 'tester3', 1),
(4, 'victor', 'vitalino', '00', '00', 'victor', 'victor', 1),
(6, 'victor', 'vitalino', '1', '00', 'victor', 'victor', 1),
(7, 'fulano', 'eita', '99', '7', 'fulano', 'fulano', 1),
(8, 'fulano', 'eita', '996', '7', 'fulano', 'fulano', 1),
(9, 'fulano de TAl', 'fulanex', '', '', 'fulano', '123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `censura`
--

CREATE TABLE `censura` (
  `idCensura` int(11) NOT NULL,
  `palavra` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `censura`
--

INSERT INTO `censura` (`idCensura`, `palavra`) VALUES
(1, 'bosta'),
(2, 'traveco'),
(3, 'cú'),
(4, 'cuzão'),
(5, 'prostituta'),
(6, 'merda'),
(7, 'mundial do palmeiras'),
(8, 'lula'),
(9, 'ladrão'),
(10, 'pinto'),
(11, 'gemidão'),
(12, 'sexo'),
(13, 'sexual'),
(14, 'travesti'),
(15, 'buceta'),
(16, 'caralho'),
(17, 'carai'),
(18, 'viado'),
(19, 'veado'),
(20, 'bixa'),
(21, 'anus');

-- --------------------------------------------------------

--
-- Table structure for table `contest`
--

CREATE TABLE `contest` (
  `idContest` int(11) NOT NULL,
  `nomeContest` varchar(200) NOT NULL,
  `status` enum('Ativo','Inativo') DEFAULT NULL,
  `loc` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contest`
--

INSERT INTO `contest` (`idContest`, `nomeContest`, `status`, `loc`) VALUES
(1, 'teste', 'Ativo', 'ifg-inhumas');

-- --------------------------------------------------------

--
-- Table structure for table `desafios`
--

CREATE TABLE `desafios` (
  `idDesafio` int(11) NOT NULL,
  `descricaoDesafio` varchar(300) NOT NULL,
  `idContest` int(11) NOT NULL,
  `status` enum('Ativo','Inativo') NOT NULL,
  `pontuacao` int(1) NOT NULL,
  `tipoDesafio` enum('Individual','Equipe') DEFAULT NULL,
  `idEquipe` int(11) DEFAULT NULL COMMENT 'Id da equipe criadora, para o caso de desafios criados por equipes. Caso seja criado pelo adm, fica em branco.\n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desafios`
--

INSERT INTO `desafios` (`idDesafio`, `descricaoDesafio`, `idContest`, `status`, `pontuacao`, `tipoDesafio`, `idEquipe`) VALUES
(1, 'teste', 1, 'Ativo', 1, 'Individual', NULL),
(2, 'novo desafio', 1, 'Ativo', 2, 'Individual', NULL),
(5, 'testando cadastro via function', 1, 'Inativo', 10, 'Individual', 0),
(6, 'Nome do personagem de sexta-feira 13', 1, 'Ativo', 12, 'Individual', NULL),
(7, 'e', 1, 'Inativo', 3, 'Equipe', 1);

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE `equipe` (
  `idEquipe` int(11) NOT NULL,
  `nomeEquipe` varchar(45) NOT NULL,
  `idContest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`idEquipe`, `nomeEquipe`, `idContest`) VALUES
(1, 'Testadores', 1),
(2, 'killers', 1);

-- --------------------------------------------------------

--
-- Table structure for table `equipe_has_candidato`
--

CREATE TABLE `equipe_has_candidato` (
  `equipe_idEquipe` int(11) NOT NULL,
  `candidato_idCandidato` int(11) NOT NULL,
  `tipo` enum('lider','membro') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='representa  participação de um candidato em uma equipe, em um contest';

--
-- Dumping data for table `equipe_has_candidato`
--

INSERT INTO `equipe_has_candidato` (`equipe_idEquipe`, `candidato_idCandidato`, `tipo`) VALUES
(1, 1, 'lider'),
(1, 2, 'membro'),
(2, 3, 'lider'),
(2, 4, 'membro'),
(2, 6, 'membro');

-- --------------------------------------------------------

--
-- Table structure for table `inativos`
--

CREATE TABLE `inativos` (
  `idInativos` int(11) NOT NULL,
  `idDesafio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inativos`
--

INSERT INTO `inativos` (`idInativos`, `idDesafio`) VALUES
(1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `mensagens`
--

CREATE TABLE `mensagens` (
  `idMensagens` int(11) NOT NULL,
  `mensagem` varchar(140) NOT NULL,
  `idCandidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mensagens`
--

INSERT INTO `mensagens` (`idMensagens`, `mensagem`, `idCandidato`) VALUES
(2, ' bom #$@#@$@#', 1),
(3, ' bom dia', 1),
(4, ' bom dia seu #$@#@$@#', 1);

-- --------------------------------------------------------

--
-- Table structure for table `respDesafio`
--

CREATE TABLE `respDesafio` (
  `idResp` int(11) NOT NULL,
  `resposta` longtext NOT NULL,
  `idDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `respDesafio`
--

INSERT INTO `respDesafio` (`idResp`, `resposta`, `idDesafio`) VALUES
(1, 'teste', 1),
(2, 'teste2', 2),
(3, 'nova resposta enviada', 1),
(4, 'indio', 5),
(5, 'jason', 6);

-- --------------------------------------------------------

--
-- Table structure for table `respostaSubmetida`
--

CREATE TABLE `respostaSubmetida` (
  `idRespSubmetida` int(11) NOT NULL,
  `respostaSubmetida` longtext,
  `idCandidato` int(11) DEFAULT NULL,
  `idDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `respostaSubmetida`
--

INSERT INTO `respostaSubmetida` (`idRespSubmetida`, `respostaSubmetida`, `idCandidato`, `idDesafio`) VALUES
(1, 'teste', 1, 1),
(2, 'teste2', 2, 1),
(3, 'teste2', 1, 2),
(4, 'ual', 1, 1),
(5, 'ual', 1, 2),
(6, 'ual', 1, 2),
(7, 'ual', 1, 2),
(8, 'ual', 9, NULL),
(9, 'ual', 9, NULL),
(10, 'ual', 9, NULL),
(11, 'ual', 9, NULL),
(12, 'ual', 9, NULL),
(13, 'ual', 9, NULL),
(14, 'ual', 9, NULL),
(15, 'ual', 9, NULL),
(16, 'ual', 9, NULL),
(17, 'ual', 9, NULL),
(18, 'ual', 9, NULL),
(19, 'ual', 9, NULL),
(20, 'ual', 9, NULL),
(21, 'ual', 9, NULL),
(22, 'ual', 9, NULL),
(23, 'teste', 9, NULL),
(24, 'uala', 1, NULL),
(25, 'uala', 1, NULL),
(26, 'uala', 1, NULL),
(27, 'uala', 1, NULL),
(28, 'uala', 1, NULL),
(29, 'teste', 1, NULL),
(30, 'teste', 1, NULL),
(31, 'teste', 1, NULL),
(32, 'teste', 1, NULL),
(33, 'teste', 1, NULL),
(34, 'teste', 1, NULL),
(35, 'teste', 1, NULL),
(36, 'teste', 1, NULL),
(37, 'uala', 1, NULL),
(41, '', 1, NULL),
(43, 'jason', 1, NULL),
(45, 'jason', 1, NULL),
(49, 'jason', 1, NULL),
(50, 'jasons', 1, NULL),
(51, 'jasons', 1, NULL),
(56, 'jason', 1, NULL),
(62, 'jason', 1, NULL),
(63, 'jason', 1, NULL),
(64, 'jason', 1, NULL),
(65, 'jason', 1, NULL),
(67, 'jason', 1, NULL),
(68, 'jasonn', 1, NULL),
(69, 'jason', 1, NULL),
(70, 'jason', 1, NULL),
(71, 'jason', 1, NULL),
(72, 'jason', 1, NULL),
(73, 'jason', 1, NULL),
(74, 'oo', 1, NULL),
(75, 'jason', 9, NULL),
(76, 'jason', 9, NULL),
(77, 'jason', 9, NULL),
(78, 'jason', 9, NULL),
(79, 'jason', 9, NULL),
(80, 'indio', 9, NULL),
(81, 'indio', 9, NULL),
(82, 'indio', 3, NULL),
(83, 'indio', 3, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_candidato`
--
CREATE TABLE `view_candidato` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_candidatos_do_contest`
--
CREATE TABLE `view_candidatos_do_contest` (
`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`Pontuação` bigint(11)
,`qtdSubmissões` bigint(21)
,`equipe` varchar(100)
,`qtdMensagens` bigint(21)
,`idCandidato` int(11)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_contestAtivo`
--
CREATE TABLE `view_contestAtivo` (
`idContest` int(11)
,`nomeContest` varchar(200)
,`status` enum('Ativo','Inativo')
,`loc` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_contests`
--
CREATE TABLE `view_contests` (
`idDesafio` int(11)
,`descricaoDesafio` varchar(300)
,`idContest` int(11)
,`status` enum('Ativo','Inativo')
,`pontuacao` int(1)
,`tipoDesafio` enum('Individual','Equipe')
,`idEquipe` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_desafios`
--
CREATE TABLE `view_desafios` (
`idDesafio` int(11)
,`descricaoDesafio` varchar(300)
,`idContest` int(11)
,`status` enum('Ativo','Inativo')
,`pontuacao` int(1)
,`tipoDesafio` enum('Individual','Equipe')
,`idEquipe` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_desafios_completo`
--
CREATE TABLE `view_desafios_completo` (
`idDesafio` int(11)
,`descricaoDesafio` varchar(300)
,`status` enum('Ativo','Inativo')
,`pontuacao` int(1)
,`tipoDesafio` enum('Individual','Equipe')
,`idEquipe` int(11)
,`certas` bigint(21)
,`todas` bigint(21)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login`
--
CREATE TABLE `view_login` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login2`
--
CREATE TABLE `view_login2` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login_adm`
--
CREATE TABLE `view_login_adm` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mensagens`
--
CREATE TABLE `view_mensagens` (
`msg` varchar(243)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ranking`
--
CREATE TABLE `view_ranking` (
`id` int(11)
,`nick` varchar(100)
,`nomeCompleto` varchar(100)
,`totalPontos` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ranking_negativo`
--
CREATE TABLE `view_ranking_negativo` (
`idCandidato` int(11)
,`nick` varchar(100)
,`total` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `view_candidato`
--
DROP TABLE IF EXISTS `view_candidato`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_candidato`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` ;

-- --------------------------------------------------------

--
-- Structure for view `view_candidatos_do_contest`
--
DROP TABLE IF EXISTS `view_candidatos_do_contest`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_candidatos_do_contest`  AS  select `candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,(select `FUNCTION_GET_PONTUACAO`(`candidato`.`idCandidato`)) AS `Pontuação`,(select count(0) from `respostaSubmetida` where (`respostaSubmetida`.`idCandidato` = `candidato`.`idCandidato`)) AS `qtdSubmissões`,(select `FUNCTION_GET_EQUIPE_DO_CANDIDATO`(`candidato`.`idCandidato`)) AS `equipe`,(select count(0) from `mensagens` where (`mensagens`.`idCandidato` = `candidato`.`idCandidato`)) AS `qtdMensagens`,`candidato`.`idCandidato` AS `idCandidato`,`candidato`.`idContest` AS `idContest` from `candidato` ;

-- --------------------------------------------------------

--
-- Structure for view `view_contestAtivo`
--
DROP TABLE IF EXISTS `view_contestAtivo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_contestAtivo`  AS  select `contest`.`idContest` AS `idContest`,`contest`.`nomeContest` AS `nomeContest`,`contest`.`status` AS `status`,`contest`.`loc` AS `loc` from `contest` where (`contest`.`status` = 'Ativo') order by `contest`.`idContest` desc limit 0,1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_contests`
--
DROP TABLE IF EXISTS `view_contests`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_contests`  AS  select `desafios`.`idDesafio` AS `idDesafio`,`desafios`.`descricaoDesafio` AS `descricaoDesafio`,`desafios`.`idContest` AS `idContest`,`desafios`.`status` AS `status`,`desafios`.`pontuacao` AS `pontuacao`,`desafios`.`tipoDesafio` AS `tipoDesafio`,`desafios`.`idEquipe` AS `idEquipe` from `desafios` order by `desafios`.`idDesafio` desc ;

-- --------------------------------------------------------

--
-- Structure for view `view_desafios`
--
DROP TABLE IF EXISTS `view_desafios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_desafios`  AS  select `desafios`.`idDesafio` AS `idDesafio`,`desafios`.`descricaoDesafio` AS `descricaoDesafio`,`desafios`.`idContest` AS `idContest`,`desafios`.`status` AS `status`,`desafios`.`pontuacao` AS `pontuacao`,`desafios`.`tipoDesafio` AS `tipoDesafio`,`desafios`.`idEquipe` AS `idEquipe` from `desafios` ;

-- --------------------------------------------------------

--
-- Structure for view `view_desafios_completo`
--
DROP TABLE IF EXISTS `view_desafios_completo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_desafios_completo`  AS  select `desafios`.`idDesafio` AS `idDesafio`,`desafios`.`descricaoDesafio` AS `descricaoDesafio`,`desafios`.`status` AS `status`,`desafios`.`pontuacao` AS `pontuacao`,`desafios`.`tipoDesafio` AS `tipoDesafio`,`desafios`.`idEquipe` AS `idEquipe`,(select count(0) from `acertoCandidato` where (`acertoCandidato`.`idDesafio` = `desafios`.`idDesafio`)) AS `certas`,(select count(0) from `respostaSubmetida` where (`respostaSubmetida`.`idDesafio` = `desafios`.`idDesafio`)) AS `todas`,`desafios`.`idContest` AS `idContest` from `desafios` ;

-- --------------------------------------------------------

--
-- Structure for view `view_login`
--
DROP TABLE IF EXISTS `view_login`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` limit 0,1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_login2`
--
DROP TABLE IF EXISTS `view_login2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login2`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` ;

-- --------------------------------------------------------

--
-- Structure for view `view_login_adm`
--
DROP TABLE IF EXISTS `view_login_adm`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login_adm`  AS  select `administrador`.`idCandidato` AS `idCandidato`,`administrador`.`nomeCompleto` AS `nomeCompleto`,`administrador`.`nick` AS `nick`,`administrador`.`matriculaIFG` AS `matriculaIFG`,`administrador`.`cpf` AS `cpf`,`administrador`.`user` AS `user`,`administrador`.`passWd` AS `passWd` from `administrador` ;

-- --------------------------------------------------------

--
-- Structure for view `view_mensagens`
--
DROP TABLE IF EXISTS `view_mensagens`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mensagens`  AS  select concat(`candidato`.`nick`,' > ',`mensagens`.`mensagem`) AS `msg`,`candidato`.`idContest` AS `idContest` from (`mensagens` join `candidato` on((`candidato`.`idCandidato` = `mensagens`.`idCandidato`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_ranking`
--
DROP TABLE IF EXISTS `view_ranking`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ranking`  AS  select `candidato`.`idCandidato` AS `id`,`candidato`.`nick` AS `nick`,`candidato`.`nomeCompleto` AS `nomeCompleto`,sum(`desafios`.`pontuacao`) AS `totalPontos` from (((`acertoCandidato` join `respostaSubmetida` on((`respostaSubmetida`.`idRespSubmetida` = `acertoCandidato`.`idRespostaSubmetida`))) join `desafios` on((`desafios`.`idDesafio` = `acertoCandidato`.`idDesafio`))) join `candidato` on((`candidato`.`idCandidato` = `respostaSubmetida`.`idCandidato`))) group by `candidato`.`idCandidato` order by `totalPontos` desc ;

-- --------------------------------------------------------

--
-- Structure for view `view_ranking_negativo`
--
DROP TABLE IF EXISTS `view_ranking_negativo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ranking_negativo`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nick` AS `nick`,count(0) AS `total` from (`respostaSubmetida` join `candidato` on((`candidato`.`idCandidato` = `respostaSubmetida`.`idCandidato`))) where (not(`respostaSubmetida`.`idRespSubmetida` in (select `acertoCandidato`.`idRespostaSubmetida` from `acertoCandidato`))) group by `respostaSubmetida`.`idCandidato` order by `total` desc limit 0,10 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  ADD PRIMARY KEY (`idAcertos`),
  ADD KEY `fk_acertoCandidato_desafios` (`idDesafio`),
  ADD KEY `fk_acertoCandidato_respostaSubmetida` (`idRespostaSubmetida`);

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idCandidato`),
  ADD UNIQUE KEY `user_UNIQUE` (`user`),
  ADD UNIQUE KEY `matriculaIFG` (`matriculaIFG`),
  ADD UNIQUE KEY `matriculaIFG_2` (`matriculaIFG`),
  ADD KEY `cpf_2` (`cpf`);

--
-- Indexes for table `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`idCandidato`),
  ADD UNIQUE KEY `matriculaIFG` (`matriculaIFG`),
  ADD UNIQUE KEY `matriculaIFG_2` (`matriculaIFG`),
  ADD KEY `cpf_2` (`cpf`),
  ADD KEY `fk_candidato_contest1_idx` (`idContest`);

--
-- Indexes for table `censura`
--
ALTER TABLE `censura`
  ADD PRIMARY KEY (`idCensura`);

--
-- Indexes for table `contest`
--
ALTER TABLE `contest`
  ADD PRIMARY KEY (`idContest`);

--
-- Indexes for table `desafios`
--
ALTER TABLE `desafios`
  ADD PRIMARY KEY (`idDesafio`),
  ADD KEY `fk_desafios_contest` (`idContest`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`idEquipe`),
  ADD KEY `fk_equipe_contest1_idx` (`idContest`);

--
-- Indexes for table `equipe_has_candidato`
--
ALTER TABLE `equipe_has_candidato`
  ADD PRIMARY KEY (`equipe_idEquipe`,`candidato_idCandidato`),
  ADD KEY `fk_equipe_has_candidato_candidato1_idx` (`candidato_idCandidato`),
  ADD KEY `fk_equipe_has_candidato_equipe1_idx` (`equipe_idEquipe`);

--
-- Indexes for table `inativos`
--
ALTER TABLE `inativos`
  ADD PRIMARY KEY (`idInativos`),
  ADD KEY `fk_inativos_desafios` (`idDesafio`);

--
-- Indexes for table `mensagens`
--
ALTER TABLE `mensagens`
  ADD PRIMARY KEY (`idMensagens`),
  ADD KEY `fk_mensagens_candidato1_idx` (`idCandidato`);

--
-- Indexes for table `respDesafio`
--
ALTER TABLE `respDesafio`
  ADD PRIMARY KEY (`idResp`),
  ADD KEY `fk_respDesafio_desafios` (`idDesafio`);

--
-- Indexes for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  ADD PRIMARY KEY (`idRespSubmetida`),
  ADD KEY `fk_respostaSubmetida_candidato` (`idCandidato`),
  ADD KEY `fk_respostaSubmetida_desafios` (`idDesafio`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  MODIFY `idAcertos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idCandidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `candidato`
--
ALTER TABLE `candidato`
  MODIFY `idCandidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `censura`
--
ALTER TABLE `censura`
  MODIFY `idCensura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `contest`
--
ALTER TABLE `contest`
  MODIFY `idContest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `desafios`
--
ALTER TABLE `desafios`
  MODIFY `idDesafio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `idEquipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `inativos`
--
ALTER TABLE `inativos`
  MODIFY `idInativos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mensagens`
--
ALTER TABLE `mensagens`
  MODIFY `idMensagens` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `respDesafio`
--
ALTER TABLE `respDesafio`
  MODIFY `idResp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  MODIFY `idRespSubmetida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  ADD CONSTRAINT `fk_acertoCandidato_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`),
  ADD CONSTRAINT `fk_acertoCandidato_respostaSubmetida` FOREIGN KEY (`idRespostaSubmetida`) REFERENCES `respostaSubmetida` (`idRespSubmetida`);

--
-- Constraints for table `candidato`
--
ALTER TABLE `candidato`
  ADD CONSTRAINT `fk_candidato_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `desafios`
--
ALTER TABLE `desafios`
  ADD CONSTRAINT `fk_desafios_contest` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `fk_equipe_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `equipe_has_candidato`
--
ALTER TABLE `equipe_has_candidato`
  ADD CONSTRAINT `fk_equipe_has_candidato_candidato1` FOREIGN KEY (`candidato_idCandidato`) REFERENCES `candidato` (`idCandidato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_equipe_has_candidato_equipe1` FOREIGN KEY (`equipe_idEquipe`) REFERENCES `equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inativos`
--
ALTER TABLE `inativos`
  ADD CONSTRAINT `fk_inativos_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mensagens`
--
ALTER TABLE `mensagens`
  ADD CONSTRAINT `fk_mensagens_candidato1` FOREIGN KEY (`idCandidato`) REFERENCES `candidato` (`idCandidato`);

--
-- Constraints for table `respDesafio`
--
ALTER TABLE `respDesafio`
  ADD CONSTRAINT `fk_respDesafio_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`);

--
-- Constraints for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  ADD CONSTRAINT `fk_respostaSubmetida_candidato` FOREIGN KEY (`idCandidato`) REFERENCES `candidato` (`idCandidato`),
  ADD CONSTRAINT `fk_respostaSubmetida_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
