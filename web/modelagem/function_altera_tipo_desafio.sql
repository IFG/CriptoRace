CREATE FUNCTION `function_altera_tipo_desafio`(
	novoTipo varchar(30),
    idD int(11)) RETURNS int(11)
BEGIN
if(novoTipo = "Individual" or novoTipo = "Equipe") then
	update desafios set tipoDesafio = novoTipo where idDesafio = idD;
    return ROW_COUNT();
else
		return 0;
end if;
END