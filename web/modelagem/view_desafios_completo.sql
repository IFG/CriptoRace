CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `criptoRace`.`view_desafios_completo` AS
    SELECT 
        `criptoRace`.`desafios`.`idDesafio` AS `idDesafio`,
        `criptoRace`.`desafios`.`descricaoDesafio` AS `descricaoDesafio`,
        `criptoRace`.`desafios`.`status` AS `status`,
        `criptoRace`.`desafios`.`pontuacao` AS `pontuacao`,
        `criptoRace`.`desafios`.`tipoDesafio` AS `tipoDesafio`,
        `criptoRace`.`desafios`.`idEquipe` AS `idEquipe`,
        (SELECT 
                COUNT(0)
            FROM
                `criptoRace`.`acertoCandidato`
            WHERE
                (`criptoRace`.`acertoCandidato`.`idDesafio` = `criptoRace`.`desafios`.`idDesafio`)) AS `certas`,
        (SELECT 
                COUNT(0)
            FROM
                `criptoRace`.`respostaSubmetida`
            WHERE
                (`criptoRace`.`respostaSubmetida`.`idDesafio` = `criptoRace`.`desafios`.`idDesafio`)) AS `todas`,
        `criptoRace`.`desafios`.`idContest` AS `idContest`
    FROM
        `criptoRace`.`desafios`