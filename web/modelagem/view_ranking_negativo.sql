CREATE VIEW `view_ranking_negativo` AS
select candidato.idCandidato, candidato.nick, count(*) as 'total' 
from respostaSubmetida inner join candidato
on candidato.idCandidato = respostaSubmetida.idCandidato 
where (respostaSubmetida.idRespSubmetida not in 
	(select idRespostaSubmetida from acertoCandidato)
)
group by respostaSubmetida.idCandidato order by total desc limit 0,10;
