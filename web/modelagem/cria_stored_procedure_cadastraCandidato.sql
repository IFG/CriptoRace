DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_cadastraCandidato`(
	IN nC VARCHAR(100),
    IN nick VARCHAR(100),
    IN matricula VARCHAR(30),
    IN cpf VARCHAR(15),
    IN usuario VARCHAR(45),
    IN pass VARCHAR(200),
    IN idC INT, OUT inserido INT)
BEGIN
	INSERT INTO `candidato`
		(nomeCompleto,nick,matriculaIFG,cpf,user,passWd,idContest)
        values
        (nC,nick,matricula,cpf,usuario,pass,idC);
        SET inserido = LAST_INSERT_ID();
END$$
DELIMITER ;

