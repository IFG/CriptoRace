<%
    HttpSession sessao = request.getSession(true);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style/style.css">
    </head>
    <body>
        <div class="container-fluid">
            <img class="img-responsive" src="img/logoG2.png" alt="CriptoRace">
            <form class="form-horizontal" action="./principal" method="POST">
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-12">
                        <a href="cadastrarCandidatos.jsp" class="btn btn-default">Candidatos</a>
                        <a href="./adm/cadastrarEquipe.jsp" class="btn btn-default">Equipes</a>
                        <a href="./adm/cadastrarDesafio.jsp" class="btn btn-default">Desafios</a>
                    </div>
                </div>
            </form>
        </div> <!-- // container -->
    </body>
</html>
