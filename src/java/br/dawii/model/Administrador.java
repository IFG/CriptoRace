package br.dawii.model;

public class Administrador {

    private final int idCandidato;
    private final String nomeCompleto;
    private final String nick;
    private final String matriculaIFG;
    private final String cpf;
    private final String user;
    private final String passWd;

    public Administrador(int id, String nome, String nick, String matricula, String cpf, String user, String pass) {
        this.idCandidato = id;
        this.nomeCompleto = nome;
        this.nick = nick;
        this.matriculaIFG = matricula;
        this.cpf = cpf;
        this.user = user;
        this.passWd = pass;
    }

    public int getIdCandidato() {
        return idCandidato;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public String getNick() {
        return nick;
    }

    public String getMatriculaIFG() {
        return matriculaIFG;
    }

    public String getCpf() {
        return cpf;
    }

    public String getUser() {
        return user;
    }

    public String getPassWd() {
        return passWd;
    }
}
