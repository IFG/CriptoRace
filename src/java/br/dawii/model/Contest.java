package br.dawii.model;

public class Contest {

    private final int idContest;
    private final String nomeContest;
    private final String status;
    private final String loc;

    public Contest(int idC, String nC, String status, String loc) {
        this.idContest = idC;
        this.nomeContest = nC;
        this.status = status;
        this.loc = loc;
    }

    public int getIdContest() {
        return idContest;
    }

    public String getNomeContest() {
        return nomeContest;
    }

    public String getStatus() {
        return status;
    }

    public String getLoc() {
        return loc;
    }
}
