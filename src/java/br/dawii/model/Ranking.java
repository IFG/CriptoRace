package br.dawii.model;

public class Ranking {

    private final Candidato candidato;
    private int pontuacao;

    public Ranking(Candidato candidato, int pontuacao) {
        this.candidato = candidato;
        this.pontuacao = pontuacao;
    }

    public Candidato getCandidato() {
        return candidato;
    }

    public int getPontuacao() {
        return pontuacao;
    }
}
