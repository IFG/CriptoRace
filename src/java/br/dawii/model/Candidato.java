package br.dawii.model;

public class Candidato {

    private final int idCandidato;
    private final String nomeCompleto;
    private final String nick;
    private final String matriculaIFG;
    private final String cpf;
    private final String user;
    private final String passWd;
    private final int idContest;

    public Candidato(int id, String nome, String nick) {
        this.idCandidato = id;
        this.nomeCompleto = nome;
        this.nick = nick;
        this.matriculaIFG = null;
        this.cpf = null;
        this.user = null;
        this.passWd = null;
        this.idContest = 0;
    }

    public Candidato(int id, String nick) {
        this.idCandidato = id;
        this.nomeCompleto = null;
        this.nick = nick;
        this.matriculaIFG = null;
        this.cpf = null;
        this.user = null;
        this.passWd = null;
        this.idContest = 0;
    }

    public Candidato(int id, String nome, String nick, String matricula, String cpf, String user, String pass, int idContest) {
        this.idCandidato = id;
        this.nomeCompleto = nome;
        this.nick = nick;
        this.matriculaIFG = matricula;
        this.cpf = cpf;
        this.user = user;
        this.passWd = pass;
        this.idContest = idContest;
    }

    public int getIdCandidato() {
        return idCandidato;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public String getNick() {
        return nick;
    }

    public String getMatriculaIFG() {
        return matriculaIFG;
    }

    public String getCpf() {
        return cpf;
    }

    public String getUser() {
        return user;
    }

    public String getPassWd() {
        return passWd;
    }

    public int getIdContest() {
        return idContest;
    }

}
