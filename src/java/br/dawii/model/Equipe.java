package br.dawii.model;

public class Equipe {

    private final int idEquipe;
    private final String nomeEquipe;
    private final int idContest;

    public Equipe(int idEquipe, String nomeEquipe, int idContest) {
        this.idEquipe = idEquipe;
        this.nomeEquipe = nomeEquipe;
        this.idContest = idContest;
    }

    public int getIdEquipe() {
        return idEquipe;
    }

    public String getNomeEquipe() {
        return nomeEquipe;
    }

    public int getIdContest() {
        return idContest;
    }

}
