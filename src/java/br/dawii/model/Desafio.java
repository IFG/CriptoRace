package br.dawii.model;

public class Desafio {

    private final int idDesafio;
    private final String descricaoDesafio;
    private final int idContest;
    private final String status;
    private final int pontuacao;
    private final String tipoDesafio;
    private final int idEquipe;

    public Desafio(int idDesafio, String descricaoDesafio, int idContest, String status, int pontuacao, String tipoDesafio, int idEquipe) {
        this.idDesafio = idDesafio;
        this.descricaoDesafio = descricaoDesafio;
        this.idContest = idContest;
        this.status = status;
        this.pontuacao = pontuacao;
        this.tipoDesafio = tipoDesafio;
        this.idEquipe = idEquipe;
    }

    public int getIdDesafio() {
        return idDesafio;
    }

    public String getDescricaoDesafio() {
        return descricaoDesafio;
    }

    public int getIdContest() {
        return idContest;
    }

    public String getStatus() {
        return status;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public String getTipoDesafio() {
        return tipoDesafio;
    }

    public int getIdEquipe() {
        return idEquipe;
    }

}
