package br.dawii.controller;

import br.dawii.dao.CandidatoDAO;
import br.dawii.model.Candidato;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "CadastrarServlet", urlPatterns = {"/cadastro"})
public class ServletCadastrarUsuario extends HttpServlet {

    private HttpSession sessao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sessao = request.getSession();

        RequestDispatcher rd = null; //setando o objeto "despachador"
        String nomeCandidato = null;
        //Colocar o mesmo parametro que esta no type do html
        String nomeCompleto = request.getParameter("nomeCompleto");
        String nick = request.getParameter("nick");
        String matriculaIFG = request.getParameter("matricula");
        String cpf = request.getParameter("cpf");
        String user = request.getParameter("user");
        String passWd = request.getParameter("pwd");

        Candidato candidato = new Candidato(0, nomeCompleto, nick, matriculaIFG, cpf, user, passWd, 1);
        CandidatoDAO dao = new CandidatoDAO();
        List<Candidato> candidatos = new ArrayList<Candidato>();
        int idC = 0;
        try {
            candidatos = dao.procura(candidato);//procurando para saber se temos alguém assim cadastrado
            //idC = candidatos.size();
            if (candidatos.size() == 0) {//opa, não existe este cabra, cadastrar-lhe-ei

                dao.salvar(candidato);//feito
                candidatos = dao.procura(candidato);
                idC = candidatos.get(0).getIdCandidato();
                nomeCandidato = candidatos.get(0).getNick();
            } else {
                idC = candidatos.get(0).getIdCandidato();
                nomeCandidato = candidatos.get(0).getNick();
            }
        } catch (Exception ex) {

        }
        rd = request.getRequestDispatcher("principal.jsp");
        //despachando para a página setada, segundo as opções acima
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
