package br.dawii.controller;

import br.dawii.dao.DesafioDAO;
import br.dawii.model.Desafio;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ServletCadastrarDesafio", urlPatterns = {"/cadastroDesafio"})
public class ServletCadastrarDesafio extends HttpServlet {

    private HttpSession sessao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        this.sessao = request.getSession();

        RequestDispatcher rd = null; //setando o objeto "despachador"
        String descricaoDesafio = request.getParameter("descricaoDesafio");
        //Colocar o mesmo parametro que esta no type do html
        String status = null;
        String pontuacao = request.getParameter("pontuacao");
        String tipoDesafio = "Individual";
        String idEquipe = null;

        Desafio desafio = new Desafio(0, descricaoDesafio, 1, status, Integer.parseInt(pontuacao), tipoDesafio, 0);
        // Candidato candidato = new Candidato(0, nomeCompleto, nick, matriculaIFG, cpf, user, passWd, 1);
        // CandidatoDAO dao = new CandidatoDAO();
        //List<Candidato> candidatos = new ArrayList<Candidato>();
        DesafioDAO desafioDAO = new DesafioDAO();
        int idC = 0;

        try {
            int retorno = desafioDAO.cadastrarDesafio(desafio);
            //Mandando o retorno para outra tela para validar se foi cadastrado ou não
            request.setAttribute("retorno", retorno);
        } catch (Exception ex) {
        }
        response.sendRedirect("./principal.jsp");
        //despachando para a página setada, segundo as opções acima
        //rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletCadastrarDesafio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletCadastrarDesafio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
