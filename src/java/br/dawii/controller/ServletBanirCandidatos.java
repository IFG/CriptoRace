package br.dawii.controller;

import br.dawii.dao.CandidatoDAO;
import br.dawii.model.Candidato;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class ServletBanirCandidatos extends HttpServlet {

    private HttpSession sessao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sessao = request.getSession();
        
        RequestDispatcher rd = null; //setando o objeto "despachador"
        String nomeCandidato = null;
        //Colocar o mesmo parametro que esta no type do html
        
        int idCandidato = Integer.parseInt(request.getParameter("idCandidato"));
        
       
        String nomeCompleto = null;
        String nick = null;
        String matriculaIFG = null;
        String cpf = null;
        String user = null;
        String passWd = null;

        Candidato candidato = new Candidato(idCandidato, nomeCompleto, nick, matriculaIFG, cpf, user, passWd, 1);
        CandidatoDAO dao = new CandidatoDAO();
        int idC = 0;
        try {
            
            dao.excluir(candidato);//procurando para saber se temos alguém assim cadastrado
            //idC = candidatos.size();
            
            
            rd = request.getRequestDispatcher("/adm/gestorUsuarios.jsp");
            
            //despachando para a página setada, segundo as opções acima
            rd.forward(request, response);
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
