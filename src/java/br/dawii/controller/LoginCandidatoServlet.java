package br.dawii.controller;

import br.dawii.dao.CandidatoDAO;
import br.dawii.model.Candidato;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "LoginCandidatoServlet", urlPatterns = {"/loginCandidatos"})
public class LoginCandidatoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        CandidatoDAO candidatoDAO = new CandidatoDAO();

        RequestDispatcher rd = null;

        String usuario = request.getParameter("usuario");
        String pass = request.getParameter("pwd");
        int idContest = 1;
        Candidato cd = new Candidato(0, null, null, null, null, usuario, pass, idContest);
        List<Candidato> listaCandidatos = candidatoDAO.procura(cd);
        if (listaCandidatos.size() > 0) {
            HttpSession sessao = request.getSession(true);
            sessao.setAttribute("idUsuario", listaCandidatos.get(0).getIdCandidato());
            sessao.setAttribute("nomeUsuario", listaCandidatos.get(0).getNomeCompleto());

            response.sendRedirect("./index.jsp");
        } else {
            request.setAttribute("LoginErrado", "Sim");
            rd = request.getRequestDispatcher("/loginCandidatos.jsp");
        }

        rd.forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(LoginCandidatoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(LoginCandidatoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
