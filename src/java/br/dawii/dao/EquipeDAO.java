package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.Equipe;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EquipeDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Equipe> listaEquipe = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_desafios`");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaEquipe.add(new Equipe(rs.getInt(1),
                        rs.getString(2), rs.getInt(3)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaEquipe;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void salvar(Object ob) throws Exception {
        Equipe equipe;
        equipe = (Equipe) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        if (equipe == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "SELECT FUNCTION_cadastra_equipe(?,?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setString(1, equipe.getNomeEquipe());
            ps.setInt(2, equipe.getIdContest());

            ps.executeQuery();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados da equipe: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

}
