package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.Desafio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DesafioDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Desafio> listaDesafios = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_desafios`");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaDesafios.add(new Desafio(rs.getInt(1),
                        rs.getString(2), rs.getInt(3),
                        rs.getString(4), rs.getInt(5),
                        rs.getString(6), rs.getInt(7)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaDesafios;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int cadastrarDesafio(Object ob) throws Exception {
        Desafio desafio;
        desafio = (Desafio) ob;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        if (desafio == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "SELECT FUNCTION_cadastra_desafio(?,?,?,?,?)";

            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement(SQL);

            ps.setString(1, desafio.getDescricaoDesafio());
            ps.setInt(2, desafio.getIdContest());
            ps.setInt(3, desafio.getPontuacao());
            ps.setString(4, desafio.getTipoDesafio());
            ps.setInt(5, desafio.getIdEquipe());

            rs = ps.executeQuery();
            //pegando o id que a function retorna se for diferente de 0 deu certo se nãp, não cadastrou.
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException sqle) {

            throw new Exception("Erro ao inserir dados do desafio: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
        return 0;
    }

    @Override
    public void salvar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
