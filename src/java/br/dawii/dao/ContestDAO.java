package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.Candidato;
import br.dawii.model.Contest;
import br.dawii.model.Ranking;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContestDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Contest> listaContests = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("select * from view_contestAtivo");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaContests.add(new Contest(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4)
                )
                );
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaContests;
    }

    public List listaRanking() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Ranking> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_ranking` ");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new Ranking(new Candidato(rs.getInt(1), rs.getString(2), rs.getString(3)), rs.getInt(4)));

            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }

    public List listaRanking2() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Ranking> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_ranking_2`");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new Ranking(new Candidato(rs.getInt(1), rs.getString(2), rs.getString(3)), rs.getInt(4)));

            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }

    public List listaRankingNegativo() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Ranking> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_ranking_negativo`  ");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new Ranking(new Candidato(rs.getInt(1), rs.getString(2)), rs.getInt(3)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }

    public List listaRankingNegativo2() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Ranking> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_ranking_negativo_2`");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new Ranking(new Candidato(rs.getInt(1), rs.getString(2)), rs.getInt(3)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void salvar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
