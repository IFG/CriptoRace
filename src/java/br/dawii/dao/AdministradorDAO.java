package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.Administrador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdministradorDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listaTodos() throws Exception {
        return null;
    }

    @Override
    public List procura(Object ob) throws Exception {
        Administrador administrador = (Administrador) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Administrador> listaAdministrador = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_login_adm` WHERE (user = ? and passWd = ?)");
            ps.setString(1, administrador.getUser());
            ps.setString(2, administrador.getPassWd());
            rs = ps.executeQuery();
            while (rs.next()) {
                listaAdministrador.add(new Administrador(rs.getInt(1),
                        rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaAdministrador;
    }

    @Override
    public void salvar(Object ob) throws Exception {
        Administrador administrador;
        administrador = (Administrador) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        if (administrador == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "SELECT FUNCTION_cadastraAdministador(?,?,?,?,?,?)";

            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement(SQL);

            ps.setString(1, administrador.getNomeCompleto());
            ps.setString(2, administrador.getNick());
            ps.setString(3, administrador.getMatriculaIFG());
            ps.setString(4, administrador.getCpf());
            ps.setString(5, administrador.getUser());
            ps.setString(6, administrador.getPassWd());

            ps.executeQuery();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados do administrador: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

}
