package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.Candidato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CandidatoDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        Candidato can = (Candidato) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        if (can == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM candidato WHERE idCandidato=?");

            ps.setInt(1, can.getIdCandidato());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Candidato> listaCandidatos = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_candidato`");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaCandidatos.add(new Candidato(rs.getInt(1),
                        rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7),
                        rs.getInt(8)
                ));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaCandidatos;
    }

    @Override
    public List procura(Object ob) throws Exception {
        Candidato candidato = (Candidato) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Candidato> listaCandidatos = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_login` WHERE (user = ? and passWd = ? and idContest = ? )");
            ps.setString(1, candidato.getUser());
            ps.setString(2, candidato.getPassWd());
            ps.setInt(3, candidato.getIdContest());
            rs = ps.executeQuery();
            while (rs.next()) {
                listaCandidatos.add(new Candidato(rs.getInt(1),
                        rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7),
                        rs.getInt(8)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaCandidatos;
    }

    @Override
    public void salvar(Object ob) throws Exception {
        Candidato candidato;
        candidato = (Candidato) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        if (candidato == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "SELECT FUNCTION_cadastraCandidato(?,?,?,?,?,?,?)";

            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement(SQL);

            ps.setString(1, candidato.getNomeCompleto());
            ps.setString(2, candidato.getNick());
            ps.setString(3, candidato.getMatriculaIFG());
            ps.setString(4, candidato.getCpf());
            ps.setString(5, candidato.getUser());
            ps.setString(6, candidato.getPassWd());
            ps.setInt(7, candidato.getIdContest());

            ps.executeQuery();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados do candidato: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    public List buscaNomes(String nome) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Candidato> listaCandidatos = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_candidato` WHERE (nomeCompleto Like ?)");
            ps.setString(1, "%" + nome + "%");

            System.out.println(ps);
            rs = ps.executeQuery();
            while (rs.next()) {
                listaCandidatos.add(new Candidato(rs.getInt(1),
                        rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7),
                        rs.getInt(8)
                ));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaCandidatos;
    }

}
